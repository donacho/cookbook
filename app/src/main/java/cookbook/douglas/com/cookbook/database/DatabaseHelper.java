package cookbook.douglas.com.cookbook.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATA_BASE_NAME = "cookbook";
    private static final int VERSION = 2;

    public DatabaseHelper(Context context) {
        super(context, DATA_BASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE RECEITA ( " +
                "ID INTEGER PRIMARY KEY, " +
                "NOME TEXT, " +
                "PORCAO DOUBLE, " +
                "INTEGER TEMPO, " +
                "INGREDIENTES TEXT, " +
                "PREPARO TEXT, " +
                "FAVORITO INTEGER, " +
                "FOTO TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}