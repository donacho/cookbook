package cookbook.douglas.com.cookbook.main;

import android.widget.ArrayAdapter;

/**
 * Created by Douglas on 08/06/2016.
 */
public class Receita {

    public String nome;
    public Float porcao;
    public Integer tempo;
    public ArrayAdapter<String> ingredientes;
    public Boolean favorito;
    public String foto;
    public String modoPreparo;

    public Receita(String nome ,Float porcao, Integer tempo, ArrayAdapter<String> ingredientes, Boolean favorito, String foto, String modoPreparo){
        super();
        this.nome = nome;
        this.porcao = porcao;
        this.tempo = tempo;
        this.ingredientes = ingredientes;
        this.modoPreparo = modoPreparo;
        this.favorito = favorito;
        this.foto = foto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Float getPorcao() {
        return porcao;
    }

    public void setPorcao(Float porcao) {
        this.porcao = porcao;
    }

    public ArrayAdapter<String> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(ArrayAdapter<String> ingredientes) {
        this.ingredientes = ingredientes;
    }

    public String getModoPreparo() {
        return modoPreparo;
    }

    public void setModoPreparo(String modoPreparo) {
        this.modoPreparo = modoPreparo;
    }

    public Integer getTempo() {
        return tempo;
    }

    public void setTempo(Integer tempo) {
        this.tempo = tempo;
    }

    public Boolean getFavorito() {
        return favorito;
    }

    public void setFavorito(Boolean favorito) {
        this.favorito = favorito;
    }
}
